#include "profile_interface.h"
#include "FastLED.h"

#define MAX_BRIGHTNESS_16 10000
#define VAL_HUE val1
//VAL2 unused as of now


/*****    VARIABLES    *****/
#define DEFAULT_HUE 30
#define STAR_SATURATION 200
const int star_skip = 1; //1 displays to all LEDS, 2 displays to every other, ...

#define minScaledRate 10
#define rateScale 2

#define MS_PER_STEP 8


class StarPixel{
  private:
    int16_t brightness;
    uint8_t rateAndTarget;

    int16_t scaledTarget_16(){
      return (int16_t)((int32_t)(rateAndTarget & 0xF) * MAX_BRIGHTNESS_16 / 15);
    }
    
    int16_t scaledRate(){
      int16_t ret = ((int16_t)(rateAndTarget >> 4) * rateScale) + minScaledRate;

      if((rateAndTarget & 0xF) == 0){
        return ret * -1;
      }
      return ret;
    }

    void newRateAndTarget(){
      rateAndTarget = random(0, 15);
      rateAndTarget = rateAndTarget << 4;
      if(brightness == 0){
        rateAndTarget += random(0, 15);
      }
    }
    
  public:
    StarPixel(){
      brightness = 0;
      newRateAndTarget();
    }
    void step_star(uint8_t steps){
      brightness += scaledRate() * steps;
      if((rateAndTarget & 0xF) == 0 && brightness <= 0){
        brightness = 0;
        newRateAndTarget();
      }
      else if((rateAndTarget & 0xF) > 0 && brightness >= scaledTarget_16()){
        brightness = scaledTarget_16();
        newRateAndTarget();
      }      
    }

    uint8_t get_brightness(uint8_t maxBrightness){
      return (uint8_t)( (uint32_t)brightness * 255 / MAX_BRIGHTNESS_16 );
    }
};


class Stars : public ProfileInterface{

private:
  StarPixel* stars;
  uint8_t leftoverMS;
  
public: 
  ProfileData data;
  CRGB *ledArr;
  int ledCount;

  Stars(){
    
  }

  void Initialize(CRGB *ledArray, int count, uint8_t brightness){
    struct ProfileData initialData;
    initialData.brightness = brightness;
    initialData.VAL_HUE = DEFAULT_HUE;
    initialData.val2 = 0;
    Initialize(ledArray, count, initialData);
  }

  void Initialize(CRGB *ledArray, int count, ProfileData initialData){
    ledArr = ledArray;
    ledCount = count;
    data = initialData;
    stars = new StarPixel[count];
  }

  int modeCount(){
    return 2;
  }

  ProfileData getData(){
    return data;
  }

  void setData(ProfileData newData){
    data = newData;
  }

  void render(int msSinceLastFrame, int mode = 0){
    int ms = msSinceLastFrame + leftoverMS;
    uint8_t steps = ms / MS_PER_STEP;
    leftoverMS = ms % MS_PER_STEP;

    if(steps == 0) return;
    
    for(int i = 0; i < ledCount; i++){
      if(i % (star_skip + mode) == 0){
        stars[i].step_star(steps);
        ledArr[i] = CHSV(data.VAL_HUE, STAR_SATURATION, stars[i].get_brightness(data.brightness));
      }
      else if(mode == 1){
        ledArr[i] = ledArr[i - 1];
      }
      else{
        ledArr[i] = CRGB::Black;
      }
    }
  }

  ~Stars(){
    delete[] stars;
  }
  
};
