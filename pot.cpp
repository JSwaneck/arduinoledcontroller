#include "FastLED.h"
class Pot{
  public:
    int16_t potMax;
    int16_t scaledMax;
    int16_t potMin;
    int16_t scaledMin;
    uint8_t analogPin;
    bool invert;

    Pot(uint8_t analogPin, bool invert, int16_t scaledMax, int16_t scaledMin, int16_t potMax, int16_t potMin){
      this->analogPin = analogPin;
      this->scaledMax = scaledMax;
      this->scaledMin = scaledMin;
      this->potMax = potMax;
      this->potMin = potMin;
      this->invert = invert;
    }

    int16_t readPot(){
      int rawPot = analogRead(analogPin);
      int16_t out;

      if(rawPot > potMax){
        potMax = rawPot;
      }
      if(rawPot < potMin){
        potMin = rawPot;
      }
      
      if(!invert){
        out = int(float(rawPot - potMin) * (scaledMax - scaledMin) / (potMax - potMin) + scaledMin);
      }
      else{
        out = int(float(potMax - rawPot) * (scaledMax - scaledMin) / (potMax - potMin) + scaledMin);
      }

      

      if(out > scaledMax)
        out = scaledMax;
      if(out < scaledMin)
        out = scaledMin;
      return out;
    }
};

