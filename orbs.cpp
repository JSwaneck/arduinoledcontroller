#include "profile_interface.h"
#include "FastLED.h"
#define VAL_ORB_HUE val1
#define VAL_ORB_SPEED val2
#define ORB_POSITION_SCALE 100

/*****    VARIABLES    *****/
//#define maxMsPerOrbPositionStep 40  //LOGARITHMIC 200 is twice as slow as 100
//#define minMsPerOrbPositionStep 3   //LOGARITHMIC 2 is twice as fast as 4
#define ORB_COUNT 12
#define ORB_RADIUS 5
#define ORB_SATURATION 255
#define MAX_VELOCITY 25
#define MIN_VELOCITY 15

struct OrbData{
  int16_t ledLocation;
  int16_t velocity;
  CRGB color;
};

#define ORB_MODE_COUNT 4
enum OrbMode{
  oneColor,
  multiColor,
  oneColorMultiVelocity,
  multiColorMultiVelocity,
};

#define COLOR_COUNT 3
const CRGB multiColors[] = {
  CRGB(200, 150, 247),
  CRGB( 73, 250, 255),
  CRGB( 50, 255, 150)
};


class Orbs : public ProfileInterface{
private:
  OrbData *orbData;
  OrbMode curMode;

  


  void Initialize(CRGB *ledArray, int count, ProfileData initialData, OrbMode mode){
    ledArr = ledArray;
    ledCount = count;
    data = initialData;
    orbData = new OrbData[ORB_COUNT];
    curMode = mode;
    for(int i = 0; i < ORB_COUNT; i++){
      orbData[i].ledLocation = int(float(ORB_POSITION_SCALE) * count * i / ORB_COUNT);
      if(curMode == oneColorMultiVelocity || curMode == multiColorMultiVelocity){
        orbData[i].velocity = random(MIN_VELOCITY, MAX_VELOCITY);
        if(i % 2 == 0){
          orbData[i].velocity *= -1; 
        }
      }
      else{
        orbData[i].velocity = MIN_VELOCITY;
      }
      
      switch(mode){
        case multiColor:
        case multiColorMultiVelocity:
          orbData[i].color = multiColors[i % COLOR_COUNT];
          break;
        case oneColor:
        case oneColorMultiVelocity:
        default:
          orbData[i].color = CHSV(data.VAL_ORB_HUE, ORB_SATURATION, 255);
          break;
      }
    }
  }

public:
  ProfileData data;
  CRGB *ledArr;
  int ledCount;

  Orbs(){
    
  }
  
  void Initialize(CRGB *ledArray, int count, uint8_t brightness){
    struct ProfileData initialData;
    initialData.brightness = brightness;
    initialData.VAL_ORB_HUE = 40;
    initialData.VAL_ORB_SPEED = 100;
    Initialize(ledArray, count, initialData);
  }

  void Initialize(CRGB *ledArray, int count, ProfileData initialData){
    Initialize(ledArray, count, initialData, 0);
  }

  ProfileData getData(){
    return data;
  }

  void setData(ProfileData newData){
    data = newData;
  }

  int modeCount(){
    return ORB_MODE_COUNT;
  }

  void render(int msSinceLastFrame, int mode = 0){
    if(static_cast<OrbMode>(mode) != curMode){
      Initialize(ledArr, ledCount, data, static_cast<OrbMode>(mode));
    }
    uint16_t orbDistance;
    
    //Render LEDS
    for(int i = 0; i < ledCount; i++){
      bool initialized = false;
      for(int r = 0; r < ORB_COUNT; r++){
        orbDistance = abs(i * ORB_POSITION_SCALE - orbData[r].ledLocation);
        if(orbDistance > ledCount * ORB_POSITION_SCALE - ORB_RADIUS * ORB_POSITION_SCALE){
          orbDistance = ledCount * ORB_POSITION_SCALE - orbDistance;
        }
        if(orbDistance <= ORB_RADIUS * ORB_POSITION_SCALE){
          CRGB temp = orbData[r].color;
          int fadeBy = 255 - int(float(data.brightness) * (ORB_RADIUS * ORB_POSITION_SCALE - orbDistance) / (ORB_RADIUS * ORB_POSITION_SCALE));
          if(initialized)
            ledArr[i] += temp.fadeLightBy(fadeBy);
          else{
            ledArr[i] = temp.fadeLightBy(fadeBy);
            initialized = true;
          }
        }
      }
      if(!initialized)
        ledArr[i] = CRGB::Black;
    }

    //Step orbs
    for(int i = 0; i < ORB_COUNT; i++){
      orbData[i].ledLocation += orbData[i].velocity;
      if(orbData[i].ledLocation > ledCount * ORB_POSITION_SCALE){
        orbData[i].ledLocation %= ledCount * ORB_POSITION_SCALE;
        //Positive velocity
        if(curMode == oneColorMultiVelocity || curMode == multiColorMultiVelocity){
          orbData[i].velocity = random(MIN_VELOCITY, MAX_VELOCITY);
        }
      }
      else if(orbData[i].ledLocation < 0){
        orbData[i].ledLocation = ledCount * ORB_POSITION_SCALE + orbData[i].ledLocation;
        //Negative velocity
        if(curMode == oneColorMultiVelocity || curMode == multiColorMultiVelocity){
          orbData[i].velocity = random(MIN_VELOCITY, MAX_VELOCITY) * -1;
        }
      }
    }

    
  }

  ~Orbs(){
    delete[] orbData;
  }
};

