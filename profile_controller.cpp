#include "rainbow.cpp"
#include "stars.cpp"
#include "orbs.cpp"

#define VALUE_VARY_RATE 20

/**
 * List of profiles to cycle through
 */
enum ProfileList
{
  rainbow,
  stars,
  orbs  
};


/**
 * Handles switching between profiles
 */
class ProfileController
{
  private:
    ProfileData profileData;
    ProfileInterface* profile = NULL;
    ProfileList curProfileType = rainbow;
    int curSubMode = 0;
    int profileBrightness = 255;
    int ledCount;
    int16_t (*getValueOnePot)();
    CRGB* leds;

    //Keeps track of value variances
    bool valueDirection[2] = {true, true};
    uint16_t valueMsCounter[2] = {0, 0};

    //Cycles value 1 and 2 from 0 - 255 and back
    //0 for val1, 1 for val2
    varyValue(int valueNum, int deltaTime){
        valueMsCounter[valueNum] += deltaTime;
        int16_t curValue;
        if(valueNum == 0) curValue = profile->getData().val1;
        else curValue = profile->getData().val2;
        
        if(valueDirection[valueNum]){
          curValue += valueMsCounter[valueNum] / VALUE_VARY_RATE;
          if(curValue >= 255){
            curValue = 255;
            valueDirection[valueNum] = false;
          }
        }
        else{
          curValue -= valueMsCounter[valueNum] / VALUE_VARY_RATE;
          if(curValue <= 0){
            curValue = 0;
            valueDirection[valueNum] = true;
          }
        }
        updateData(valueNum + 1,curValue,profile);
        valueMsCounter[valueNum] %= VALUE_VARY_RATE;
    }
  
  public:

    bool varyVal1 = false;
    bool varyVal2 = false;
  
    ProfileController(CRGB* leds, int ledCount, int16_t (*getValueOnePot)()){
      this->ledCount = ledCount;
      this->leds = leds;
      this->getValueOnePot = getValueOnePot;
      profile = new Rainbow();
      profile->Initialize(leds, ledCount, profileBrightness);
    }

    ~ProfileController(){
      delete profile;
    }

    void render(int msSinceLastFrame){
      profile->render(msSinceLastFrame, curSubMode);
      if(varyVal1){
        varyValue(0, msSinceLastFrame);
      }
      else{
        updateData(1, getValueOnePot(), profile);
      }
      if(varyVal2){
        varyValue(1, msSinceLastFrame);
      }
    }
  
    //Steps to the next sub-mode or profile once all sub-modes are cycled through
    void nextProfile(){
      curSubMode++;
      if(curSubMode >= profile->modeCount()){
        curSubMode = 0;
        switch(curProfileType){
          case rainbow:
            delete profile;
            profile = new Stars();
            curProfileType = stars;
            break;
          case stars:
            delete profile;
            profile = new Orbs();
            curProfileType = orbs;
            break;
          case orbs:
          default:
            delete profile;
            profile = new Rainbow();
            curProfileType = rainbow;
        }
        profile->Initialize(leds, ledCount, profileBrightness);
      }
      if(curProfileType == rainbow && curSubMode == 0){
        varyVal1 = true; varyVal2 = true;
      }
      else{
        varyVal1 = false; varyVal2 = false;
      }
    }
    
    /**
     * Updates one field of the profile data
     * valNum : 0 to change brightness, 1 for val1, 2 for val2
     * newVal : new value
     */
    void updateData(int valNum, uint8_t newVal, ProfileInterface *p){
      ProfileData data = p->getData();
    
      switch(valNum){
        case 0:
          data.brightness = newVal;
          break;
        case 1:
          data.val1 = newVal;
          break;
        case 2:
          data.val2 = newVal;
          break;
        default:
          return;
      }
      
      p->setData(data);
    }
};

