#include "profile_interface.h"
#include "FastLED.h"
class Waves : public ProfileInterface{


//TODO: This does not implement profile_interface yet


private:
  int startingHue;
  int ledHue;
  int startingBrightness;
  int ledBrightness;
  int ledStep;
  int hueStep;
  int startingLedStep;
  
public: 
  ProfileData data;
  CRGB *ledArr;
  int ledCount;

  int loopStep;
  uint8_t saturation;
  uint8_t maxBrightness;
  
  Waves(CRGB *ledArray, int count, int loopStep, int ledStep, int hueStep, uint8_t saturation, uint8_t maxBrightness, uint8_t startingHue){
    ledArr = ledArray;
    ledCount = count;
    this->ledStep = ledStep;
    this->loopStep = loopStep;
    this->saturation = saturation;
    this->maxBrightness = maxBrightness;
    this->startingHue = startingHue;
    this->hueStep = hueStep;
    startingLedStep = ledStep;
    startingBrightness = 0;
    ledHue = 0;
    
  }
  
  solidWaves(){
    ledStep = startingLedStep;
    ledBrightness = startingBrightness;
    for(int i = 0; i < ledCount; i++){
      ledArr[i] = CHSV(startingHue, saturation, ledBrightness);
      ledBrightness += ledStep;
      if(ledBrightness >= maxBrightness){
        ledBrightness = maxBrightness;
        ledStep = -ledStep;
      }
      else if(ledBrightness <= 0){
        ledBrightness = 0;
        ledStep = -ledStep;
      }
    }
    startingBrightness += loopStep;
    if(startingBrightness >= maxBrightness){
      startingBrightness = maxBrightness;
      loopStep = -loopStep;
      startingLedStep = -startingLedStep;
    }
    else if(startingBrightness <= 0){
      startingBrightness = 0;
      loopStep = -loopStep;
      startingLedStep = -startingLedStep;
    }
  }
  
  rainbowWaves(){
    startingHue += hueStep;
    startingHue = startingHue % 255;
    solidWaves();
  }
  
};
