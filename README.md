# Arduino LED Controller #
* Control a strip of addressable RGB LEDs such as WS2813
* Arduino and external button required
* [Install FastLED](https://github.com/FastLED/FastLED)


## Setup and install

1. Download this repo
2. Download FastLED
3. Place "FastLED" folder in "libraries" if using ArduinoIDE
4. Configure arduino ports in ArduinoLEDController.ino   
5. Upload to arduino and run!


### Setup file structure
    .
    ├── ArduinoLEDController	      # This repo
    ├── libraries                     # Arduino IDE library folder
    │   └── fastLED                   # FastLED
    └── ...

## Making display profiles

1. Make a class that implements "ProfileInterface" in profile_interface.h
2. Add it to changeProfile in ArduinoLEDController.ino