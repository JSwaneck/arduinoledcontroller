#ifndef PROFILE_INTERFACE_H
#define PROFILE_INTERFACE_H
#include "FastLED.h"

/**
 * Profile interface for each display mode
 */



/**
 * This is what gets passed to the profile by the controller
 * Limit output brightness to the given brightness
 * Use val1/2 to control any other aspect of the profile
 * such as hue, animation speed, etc
 */
struct ProfileData{
  uint8_t brightness;
  uint8_t val1;
  uint8_t val2;
};


/**
 * All display profiles implement this
 */
class ProfileInterface{
  public:
  
    //Data passed from the controller - Can change dynamically
    ProfileData data; 
    
    //Initializes with default values
    virtual void Initialize(CRGB *ledArray, int count, uint8_t brightness = 255) = 0; 

    //Initializes with given initial values
    virtual void Initialize(CRGB *ledArray, int count, ProfileData initialData) = 0;

    //Modify the ledArray here to color the strip - The controller will apply the colors
    virtual void render(int msSinceLastFrame, int mode = 0) = 0;

    //Destructor is called whenever the profile changes to free memory
    virtual ~ProfileInterface(){}

    //How many sub modes the profile has - All sub modes will cycle using the external button
    virtual int modeCount() = 0; //

    //Gets the ProfileData
    virtual ProfileData getData();

    //Sets the ProfileData
    virtual void setData(ProfileData newData);
};

#endif
