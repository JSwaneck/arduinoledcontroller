#include "FastLED.h"
class Button{
  private:
    uint8_t prevState;
    void (*onClick)();
    void (*onLongPress)();
    uint16_t msForHold;
    uint8_t digitalPin;
    int currentMs;
    bool pastLongHold;
    bool invertHighLow;

  bool isHigh(uint8_t state){
    return (invertHighLow && state == LOW) || (!invertHighLow && state == HIGH);
  }
    
  public:
    Button(uint8_t digitalPin, void (*onClickCallback)(), void (*onLongPressCallback)(), bool invertHighLow){
      this->digitalPin = digitalPin;
      onClick = onClickCallback;
      onLongPress = onLongPressCallback;
      this->msForHold = 700;
      this->invertHighLow = invertHighLow;
      currentMs = 0;
      prevState == LOW;
      pastLongHold = false;
    }

    checkState(int msSinceLastUpdate){
      uint8_t currentState = digitalRead(digitalPin);
      if(isHigh(prevState) && !pastLongHold){
        currentMs += msSinceLastUpdate;
        if(currentMs >= msForHold){
          pastLongHold = true;
          if(onLongPress){     
            onLongPress();
          }
        }
        else if(!isHigh(currentState)){
          if(onClick){
            onClick();
          }
        }
      }
      else if(!isHigh(prevState) && isHigh(currentState)){
        currentMs = msSinceLastUpdate;
        pastLongHold = false;
      }
      prevState = currentState;
    }
};

