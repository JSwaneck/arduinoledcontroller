/**
 * Arduino LED Controller
 * by Jacob Swaneck 
 * jpswaneck@gmail.com
 * 
 * Requires FastLED to compile
 *  https://github.com/FastLED/FastLED
 */

#include "FastLED.h"
#include "button.cpp"
#include "pot.cpp"
#include "profile_controller.cpp"

/* Arduino pins and LED type */
#define LED_DATA_PIN    12
#define CHIPSET         WS2813
#define COLOR_ORDER     GRB
#define BTN_LEFT_PIN    10
#define BTN_CENTER_PIN  9
#define BTN_RIGHT_PIN   8
#define POT_LEFT_PIN    7
#define POT_RIGHT_PIN   6


/* Variables */
#define NUM_LEDS 149
#define MS_PER_FRAME 8

/* Led array and profile */
CRGB leds[NUM_LEDS];
bool isOn = true;
int fastLedBrightness = 255;

//Oscilate two values
bool varyVal1 = true;
bool varyVal2 = true;

ProfileController* profileController;

/**
 * Colors all LEDs. Does not paint FastLED
 */
void colorAll(CRGB color){
  for(int i = 0; i < NUM_LEDS; i++){
    leds[i] = color;
  }
}

/**
 * Callback to change the profile
 */
void changeProfile(){
  profileController->nextProfile();
}
void (*changeProfilePtr)() = &changeProfile;

void toggleOnOff(){
  isOn = !isOn;
  if(!isOn){
    colorAll(CRGB::Black);
    FastLED.show();
  }
}
void (*toggleOnOffPtr)() = &toggleOnOff;

Button* modeButton = new Button(BTN_CENTER_PIN, changeProfilePtr, NULL, false);
Button* powerButton = new Button(BTN_LEFT_PIN, toggleOnOffPtr, NULL, false);
Pot* brightnessPot = new Pot(POT_RIGHT_PIN, true, 255, 0, 1024, 0);
Pot* valueOnePot = new Pot(POT_LEFT_PIN, true, 255, 0, 1024, 0);

uint16_t potOneValue(){
  return valueOnePot->readPot();
}
uint16_t (*potOneValuePtr)() = &potOneValue;

void setup() {
  Serial.begin(9600);
  Serial.println("Setup Running...");
  randomSeed(analogRead(3));
  delay(100);
  
  pinMode(BTN_LEFT_PIN, INPUT);
  pinMode(BTN_CENTER_PIN, INPUT);
  pinMode(BTN_RIGHT_PIN, INPUT);
  FastLED.addLeds<CHIPSET, LED_DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness( fastLedBrightness );
  colorAll(CRGB::Black);

  profileController = new ProfileController(leds, NUM_LEDS, potOneValuePtr);
  
  Serial.println("Setup Complete");
  delay(100);
}

unsigned long lastMillis = 0;

void loop() {
  unsigned long currentMillis = millis();
  unsigned long deltaTime = currentMillis - lastMillis;
  if(deltaTime >= MS_PER_FRAME){
    lastMillis = currentMillis;
    modeButton->checkState(deltaTime);
    powerButton->checkState(deltaTime);

    if(isOn){
      currentMillis = millis();
      profileController->render(deltaTime);
      FastLED.show();

      if(brightnessPot->readPot() != fastLedBrightness){
        fastLedBrightness = brightnessPot->readPot();
        FastLED.setBrightness(fastLedBrightness);
      }
      
    }
    
  }
}

