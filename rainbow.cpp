#include "profile_interface.h"
#include "FastLED.h"
#define VAL_LOOP_SPEED val1
#define VAL_LED_SPEED val2
#define LOOP_ACCURACY 10

/*****    VARIABLES    *****/
#define maxMsPerLoopHueStep 40  //LOGARITHMIC 200 is twice as slow as 100
#define minMsPerLoopHueStep 3   //LOGARITHMIC 2 is twice as fast as 4
#define ledCounterConstant 190
#define RAINBOW_SATURATION 255

#define DESK_LEDS 69 //This is for my room on submode 2
#define BED_LEDS 80

class Rainbow : public ProfileInterface{

private:
  uint16_t startingHue;
  uint16_t ledHue;
  uint16_t loopCounter;
  uint16_t ledCounter;
  const static float log2 = log(2);
  const static float log2MaxLoop = log(maxMsPerLoopHueStep) / log2;

  uint16_t mapValLoopSpeedToLoopRange(){
    return int(LOOP_ACCURACY * (pow(2,(255 - data.VAL_LOOP_SPEED) * log2MaxLoop / 255) + minMsPerLoopHueStep - 1));
  }

  void incrementLoopHue(int msSinceLastFrame){
    loopCounter += msSinceLastFrame * LOOP_ACCURACY;
    startingHue += loopCounter / mapValLoopSpeedToLoopRange();
    startingHue %= 256;
    loopCounter %= mapValLoopSpeedToLoopRange();
  }

  void incrementLedHue(){
     ledCounter += ledCounterConstant;
     ledHue += ledCounter / (256 - data.VAL_LED_SPEED);
     ledHue %= 256;
     ledCounter %= (256 - data.VAL_LED_SPEED);
  }
  
public: 
  ProfileData data;
  CRGB *ledArr;
  int ledCount;

  Rainbow(){
    
  }
  
  void Initialize(CRGB *ledArray, int count, uint8_t brightness){
    struct ProfileData initialData;
    initialData.brightness = brightness;
    initialData.VAL_LOOP_SPEED = 100;
    initialData.VAL_LED_SPEED = 100;
    Initialize(ledArray, count, initialData);
  }

  void Initialize(CRGB *ledArray, int count, ProfileData initialData){
    ledArr = ledArray;
    ledCount = count;
    data = initialData;
    startingHue = 0;
    ledHue = 0;
    loopCounter = 0;
    ledCounter = 0;
  }

  ProfileData getData(){
    return data;
  }

  void setData(ProfileData newData){
    data = newData;
  }

  int modeCount(){
    return 2;
  }

  void render(int msSinceLastFrame, int mode = 0){
    ledHue = startingHue;
    for(int i = 0; i < ledCount; i++){
      if(mode == 2 && i == DESK_LEDS)
        ledHue = startingHue;
      ledArr[i] = CHSV(ledHue, RAINBOW_SATURATION, data.brightness);
      if(mode != 1)
        incrementLedHue();
      
    }
    incrementLoopHue(msSinceLastFrame);
  }
  
};
